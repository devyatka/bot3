name := "telegrambot"

version := "0.1"

scalaVersion := "2.12.4"

libraryDependencies ++= Seq(
  "org.tpolecat" %% "atto-core" % "0.6.2-M1",
  "com.jsuereth" %% "scala-arm" % "2.0",
  "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4",
  "org.scalatest" %% "scalatest" % "3.0.5" % "test",
  "info.mukel" %% "telegrambot4s" % "3.0.14"
)

resolvers += Resolver.mavenLocal