package com.telegrambot

import info.mukel.telegrambot4s.models.User

object Bot {
  def response(command: String, user: User): String ={
    val result = CommandParser(command)
    result match{
      case c: Matcher.CreatePoll =>
        CommandsImplementation.createPoll(c.name, c.isAnonymous, c.isAfterStop, c.startDate, c.stopDate, user)
      case _: Matcher.ListPolls => CommandsImplementation.listPolls()
      case _: Matcher.Help => CommandsImplementation.help()
      case c: Matcher.DeletePoll => CommandsImplementation.deletePoll(c.id,user)
      case c: Matcher.StartPoll => CommandsImplementation.startPoll(c.id, user)
      case c: Matcher.StopPoll => CommandsImplementation.stopPoll(c.id, user)
      case c: Matcher.ShowResult => CommandsImplementation.showResult(c.id)
      case c: Matcher.BeginContext => CommandsImplementation.beginContext(c.id, user)
      case _: Matcher.EndContext => CommandsImplementation.endContext(user)
      case c: Matcher.AddQuestion => CommandsImplementation.addQuestion(c.question,c.questionType,c.variants, user)
      case c: Matcher.DeleteQuestion => CommandsImplementation.deleteQuestion(c.id, user)
      case c: Matcher.AnswerOpen => CommandsImplementation.answerOpen(c.id, c.answer, user)
      case c: Matcher.AnswerInt => CommandsImplementation.answerInt(c.id, c.indexes, user)
      case _: Matcher.ViewPoll => CommandsImplementation.viewPoll(user)
      case _: Matcher.Failure => "Invalid command"
    }
  }
}


