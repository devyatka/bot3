package com.telegrambot

import scala.collection.immutable.HashMap

object PollRepo{
  private var repo: PollRepo = new PollRepo
  def clear(): Unit = repo = new PollRepo

  def getPollId: Int = repo.pollId.next()

  def getPoll(id: Int): Option[Poll] = repo.polls.get(id)

  def setPoll(id: Int, poll: Poll): Unit = repo = repo.copy(polls = repo.polls + (id -> poll))

  def addPoll(poll:Poll): Int = {
    val id = getPollId
    setPoll(id, poll)
    id
  }

  def deletePoll(id: Int): Unit = repo = repo.copy(polls = repo.polls - id)

  def getPollList: String = repo.polls.map{case (id, poll) => s"Id: $id Name: ${poll.name}\n"}.mkString("")

  def isEmpty: Boolean = repo.polls.isEmpty
}

case class PollRepo(private val polls: HashMap[Int, Poll] = new HashMap[Int, Poll],
                    private val pollId: Iterator[Int] = Stream.from(0).iterator)

object Context{
  private var usersInContext: HashMap[Int, Int] = new HashMap[Int,Int]

  def getPollId(userId: Int): Option[Int] = usersInContext.get(userId)

  def setUserContext(userId: Int, pollId: Int): Unit = usersInContext += (userId -> pollId)

  def deleteUserContext(userId: Int): Unit = usersInContext -= userId

  def clear(): Unit = usersInContext = new HashMap[Int, Int]
}