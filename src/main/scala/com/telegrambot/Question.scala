package com.telegrambot

import scala.collection.immutable.HashMap
import info.mukel.telegrambot4s.models.User

object QuestionType extends Enumeration {
  type QuestionType = Value
  val Open, Choice, Multi = Value
}

case class Question(name: String, questionType: QuestionType.QuestionType, anonymous: Boolean, variants: Seq[String],
                    votedUsers: Seq[User] = Seq.empty, var votes: HashMap[String, Int] = HashMap.empty,
                    userVotes: HashMap[User, Seq[String]] = HashMap.empty) {

  def voteOpen(user: User, answer: String): Question ={
    val newVariants = variants :+ answer
    val newVotedUsers = votedUsers :+ user
    val newUserVotes =  if (anonymous) userVotes else userVotes + (user ->
      (userVotes.getOrElse(user, Seq[String]()) :+ answer))
    this.copy(variants = newVariants, votedUsers = newVotedUsers, userVotes = newUserVotes)
  }

  def voteChoice(user: User, answer: String): Question = {
    votes += answer -> (votes.getOrElse(answer, 0) +1)
    val newVotedUsers = votedUsers :+ user
    val newUserVotes =  if (anonymous) userVotes else userVotes + (user ->
    (userVotes.getOrElse(user, Seq[String]()) :+ answer))
    this.copy(votedUsers = newVotedUsers, userVotes = newUserVotes)
  }

  def voteMulti(user: User, indexes: Seq[Int]): Question = {
    indexes.foreach(index => { votes += variants(index) -> (votes.getOrElse(variants(index),0)+1)})
    val newVotedUsers = votedUsers :+ user
    val answers: Seq[String] = indexes.map(index => variants(index))
    val newUserVotes: HashMap[User, Seq[String]] = if (anonymous) userVotes else userVotes + (user ->
      (userVotes.getOrElse(user, Seq[String]()) ++ answers))

    this.copy(votedUsers = newVotedUsers, userVotes = newUserVotes)
  }
}

