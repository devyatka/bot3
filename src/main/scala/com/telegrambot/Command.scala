package com.telegrambot

import java.time.{LocalDateTime => Date}

import com.telegrambot.QuestionType.QuestionType

trait Command

object Matcher{
  case class CreatePoll(name: String, isAnonymous: Option[Boolean], isAfterStop: Option[Boolean],
                        startDate: Option[Date], stopDate: Option[Date]) extends Command

  case class Help() extends Command

  case class ListPolls() extends Command

  case class DeletePoll(id: Int) extends Command

  case class StartPoll(id: Int) extends Command

  case class StopPoll(id: Int) extends Command

  case class ShowResult(id: Int) extends Command

  case class BeginContext(id: Int) extends Command

  case class EndContext() extends Command

  case class ViewPoll() extends Command

  case class AddQuestion(question: String, questionType: QuestionType, variants: Seq[String]) extends Command

  case class DeleteQuestion(id: Int) extends Command

  case class AnswerOpen(id: Int, answer: String) extends Command

  case class AnswerInt(id: Int, indexes: Seq[Int]) extends Command

  case class Failure() extends Command
}

