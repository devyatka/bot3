package com.telegrambot

import scala.util.parsing.combinator.RegexParsers
import java.time._
import QuestionType.QuestionType


class CommandParser extends RegexParsers {
  def bool: Parser[Boolean] = ("yes" | "afterstop") ^^ (_ => true) | ("no" | "continuous") ^^ (_ => false)

  def timeNum: Parser[Int] = """\d{2}""".r ^^ { s => s.toInt }

  def date: Parser[LocalDate] = (timeNum <~ ".") ~ (timeNum <~ ".") ~ timeNum ^^ { case year ~ month ~ day => {
    LocalDate.of(2000 + year, month, day)}}

  def time: Parser[LocalTime] = (timeNum <~ ":") ~ (timeNum <~ ":") ~ timeNum ^^ { case hours ~ minutes ~ seconds => {
    LocalTime.of(hours, minutes, seconds)}}

  def datetime: Parser[LocalDateTime] = time ~ date ^^ { case time ~ date => LocalDateTime.of(date, time)}

  def word: Parser[String] = "((".? ~ """(\w)+""".r ~ "))".? ^^ { case openbr ~ word ~ closebr =>
    openbr.map(_=>"(").getOrElse("") + word + closebr.map(_=>")").getOrElse("")}

  def int: Parser[Option[Int]] = """\d+""".r ^^ { str => util.Try(str.toInt).toOption}

  def questionType: Parser[QuestionType] = "(" ~> "open" <~ ")" ^^ {_ => QuestionType.Open} |
    "(" ~> "choice" <~ ")" ^^ {_ => QuestionType.Choice} |
    "(" ~> "multi" <~ ")" ^^ {_ => QuestionType.Multi}

  def beginContext: Parser[Command] = "/begin" ~> "(" ~> int <~ ")" ^^ { i =>
    i.map {id => Matcher.BeginContext(id)}.getOrElse(Matcher.Failure())}

  def endContext: Parser[Command] = "^/end$".r ^^ {_ => Matcher.EndContext()}

  def viewPoll: Parser[Command] = "^/view$".r ^^ {_ => Matcher.ViewPoll()}

  def addQuestion: Parser[Command] = {
    val questionName = Parser("("~>rep1(word)<~")")
    val variants = rep1(questionName)
    "/add_question" ~> questionName ~ questionType.? ~ variants.? ^^ { case question ~ t ~ ans =>
      val variants: Seq[String] = ans.getOrElse(Seq.empty).map(s => s.mkString(""))
      val qType: QuestionType = t.getOrElse(QuestionType.Open)
      if (qType == QuestionType.Open && variants.nonEmpty || qType != QuestionType.Open && variants.isEmpty) Matcher.Failure()
      else Matcher.AddQuestion(question.mkString(" "), qType, variants)}
  }

  def deleteQuestion: Parser[Command] = "/delete_question" ~> "(" ~> int <~ ")" ^^ { i =>
    i.map {id => Matcher.DeleteQuestion(id)}.getOrElse(Matcher.Failure())}

  def answerOpen: Parser[Command] = {
    val idParser = Parser("(" ~> int <~ ")")
    val argsParser =  Parser("(" ~> rep1(word) <~ ")")
    "/answer" ~> idParser ~ argsParser ^^ {case id ~ args =>
      id.map {i => Matcher.AnswerOpen(i, args.mkString(" "))}.getOrElse(Matcher.Failure())}
  }

  def answerInt: Parser[Command] = {
    val idParser = Parser("(" ~> int <~ ")")
    val argsParser = rep1(idParser)
    "/answer" ~> idParser ~ argsParser ^^ {case id ~ args =>
      if ((id :: args).forall(i => i.isDefined)) Matcher.AnswerInt(id.get, args.map(index => index.get))
      else Matcher.Failure()
    }
  }

  def createPoll: Parser[Command] = {
    val pollName = Parser("(" ~> rep1(word) <~ ")")
    val anonymity = Parser("(" ~> bool <~ ")")
    val continuous = anonymity
    val start = Parser("(" ~> datetime <~ ")")
    val stop = start
    ("/create_poll" ~> pollName) ~ anonymity.? ~ continuous.? ~ start.? ~ stop.? ^^ {
      case name ~ isAnonymous ~ isVisible ~ startTime ~ stopTime => {
      Matcher.CreatePoll(name.mkString(" "), isAnonymous, isVisible, startTime, stopTime)}}}

  def listPolls: Parser[Command] = "/list" ^^ { _ => Matcher.ListPolls()}

  def deletePoll: Parser[Command] = "/delete_poll" ~> "(" ~> int <~ ")" ^^ { i =>
    i.map {id => Matcher.DeletePoll(id)}.getOrElse(Matcher.Failure())}

  def startPoll: Parser[Command] = "/start_poll" ~> "(" ~> int <~ ")" ^^ { i =>
    i.map {id => Matcher.StartPoll(id)}.getOrElse(Matcher.Failure())}

  def stopPoll: Parser[Command] = "/stop_poll" ~> "(" ~> int <~ ")" ^^ { i =>
      i.map {id => Matcher.StopPoll(id)}.getOrElse(Matcher.Failure())}

  def pollResult: Parser[Command] = "/result" ~> "(" ~> int <~ ")" ^^ { i =>
    i.map {id => Matcher.ShowResult(id)}.getOrElse(Matcher.Failure())}

  def help: Parser[Command] = "/help" ^^ { _ => Matcher.Help()}

  def apply(input: String): Command =
    parse(help | createPoll | listPolls | deletePoll | startPoll | stopPoll | pollResult | beginContext |
      endContext | viewPoll | addQuestion | deleteQuestion | answerInt | answerOpen, input) match{
      case Success(response,_) => response
      case NoSuccess(_,_) => Matcher.Failure()}
}

object CommandParser extends CommandParser