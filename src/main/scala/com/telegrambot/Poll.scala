package com.telegrambot

import info.mukel.telegrambot4s.models.User
import java.time.{LocalDateTime => Date}
import scala.collection.immutable.HashMap


case class Poll(private val questions: HashMap[Int,Question], name: String, creator: User, isAnonymous: Boolean,
                isAfterStop: Boolean, startDate: Option[Date], stopDate: Option[Date]) {
  override def toString: String = {
    val dateStart: String = startDate.map {start =>
      s"${start.getYear}.${start.getMonthValue}.${start.getDayOfMonth}"}.getOrElse("not set")
    val timeStart: String = startDate.map {start =>
      s"${start.getHour}:${start.getMinute}:${start.getSecond}"}.getOrElse("not set")
    val dateStop: String = stopDate.map {stop =>
      s"${stop.getYear}.${stop.getMonthValue}.${stop.getDayOfMonth}"}.getOrElse("not set")
    val timeStop: String = stopDate.map {stop =>
      s"${stop.getHour}:${stop.getMinute}:${stop.getSecond}"}.getOrElse("not set")
    s"\nName: $name\nAnonymity: $isAnonymous\nIs after stop: $isAfterStop\n" +
      s"Start date:\n\tdate: $dateStart\n\ttime: $timeStart\n" +
      s"Stop date:\n\tdate: $dateStop\n\ttime: $timeStart\n"}

  def active(nowTime: Date): Boolean =
    startDate.exists { start => {
      (start.isBefore(nowTime) || start.isEqual(nowTime)) &&
        (stopDate.isEmpty || stopDate.get.isAfter(nowTime) || stopDate.get.isEqual(nowTime))
    }
    }

  def finished(nowTime: Date): Boolean = stopDate.exists { date => date.isBefore(nowTime) || date.isEqual(nowTime)}

  def getQuestion(id: Int): Option[Question] = questions.get(id)

  def addQuestion(question: Question): HashMap[Int, Question] = questions + (getMinFreeId -> question)

  def addVoteQuestion(questionId: Int, question: Question): HashMap[Int, Question] =
    questions + (questionId -> question)

  def deleteQuestion(questionId: Int): HashMap[Int, Question] = questions - questionId

  def getResult: String = {
    questions.map { case (_, question) =>
        question.questionType match {
          case QuestionType.Open => s"${question.name}:\n" + question.variants.mkString("\n")
          case _ => s"${question.name}:\n" + question.votes.map { case (variant, count) => s"$variant: $count\n" }.mkString("")
        }
      }.mkString("\n")
  }

  private def getMinFreeId: Int = {
    (0 to questions.keySet.size).foreach(i => {
      if (!questions.keySet.contains(i))
        return i
    })
    questions.keySet.max + 1
  }
}
