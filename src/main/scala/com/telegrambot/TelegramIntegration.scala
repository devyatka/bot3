package com.telegrambot

import info.mukel.telegrambot4s.api.declarative.Commands
import info.mukel.telegrambot4s.api.{Polling, TelegramBot}
import info.mukel.telegrambot4s.methods._
import info.mukel.telegrambot4s.models.{Message, User}
import scala.io.Source


object TelegramIntegration extends TelegramBot with Polling with Commands {

  lazy val token: String = Source.fromFile("token.txt").getLines.mkString

  override def receiveMessage(msg: Message): Unit = {
    val name = s"${msg.from.get.firstName} ${msg.from.get.lastName.getOrElse("")}".trim
    val user = User(id = msg.from.get.id, isBot = false, firstName = name)
    for (text <- msg.text) {
      println(s"Recieved: '$text', from: '$name', id: ${msg.from.get.id}")
      request(SendMessage(msg.source, Bot.response(text, user), Some(ParseMode.Markdown)))
    }
  }

  def main(a: Array[String]): Unit = {
    println("Bot is activated")
    run()
  }
}

