package com.telegrambot

import java.time.{LocalDateTime => Date}
import info.mukel.telegrambot4s.models.User
import com.telegrambot.QuestionType.QuestionType
import scala.collection.immutable.HashMap

object CommandsImplementation {
  def createPoll(name: String, isAnonymous: Option[Boolean], isAfterStop: Option[Boolean],
                 startDate: Option[Date], stopDate: Option[Date], user: User): String = {
    val id: Int = PollRepo.addPoll(Poll(new HashMap[Int, Question], name, user, isAnonymous.getOrElse(false),
      isAfterStop.getOrElse(false), startDate, stopDate))
    s"Id of $name poll is: $id"
  }

  def help(): String = {
    "\tAvailable commands:" +
      "\n/help" +
      "\n/create\\_poll (poll\\_name) [(yes/no) (afterstop/continuous) [(start_time) (stop_time)]]" +
      "\n/list" +
      "\n/delete\\_poll (poll\\_id) " +
      "\n/start\\_poll (poll\\_id) " +
      "\n/stop\\_poll (poll\\_id) " +
      "\n/result (poll\\_id) " +
      "\n\n Available context commands: " +
      "\n/begin (poll\\_id) " +
      "\n/view " +
      "\n/add\\_question (question\\_name) (open/choice/multi) [(variants)] " +
      "\n/delete\\_question (question\\_id) " +
      "\n/answer (question\\_id) (choice\\_id/answer) " +
      "\n/end\n"
  }

  def listPolls(): String = {
    if (PollRepo.isEmpty)
      s"There are no polls:\n"
    else
      s"There are following polls:\n" + PollRepo.getPollList
  }

  def deletePoll(id: Int, user: User): String = {
    PollRepo.getPoll(id).map { poll =>
      if (poll.creator.id == user.id) {
        PollRepo.deletePoll(id)
        Context.getPollId(user.id).foreach { _ => Context.deleteUserContext(user.id) }
        s"Poll '${poll.name}' with id $id has been deleted\n"
      }
      else "You can't delete not yours poll\n"
    }.getOrElse(s"Sorry, the poll with id: $id has not been found\n")
  }

  def startPoll(id: Int, user: User): String = {
    PollRepo.getPoll(id).map { poll =>
      if (poll.creator.id == user.id) {
        if (poll.active(Date.now()))
          s"The poll with id $id is already started\n"
        else {
          poll.startDate.map { _ =>
            if (poll.finished(Date.now()))
              s"Poll $id already worked\n"
            else
              s"Poll $id would start by itself\n"
          }.getOrElse({
            PollRepo.setPoll(id, poll.copy(startDate = Some(Date.now())))
            s"Poll $id has been started\n"
          })
        }
      }
      else "You can't start not yours poll\n"
    }.getOrElse(s"Sorry, the poll with id: $id has not been found\n")
  }

  def stopPoll(id: Int, user: User): String = {
    PollRepo.getPoll(id).map { poll =>
      if (poll.creator.id == user.id) {
        if (poll.finished(Date.now()))
          s"The Poll $id is already finished\n"
        else if (!poll.active(Date.now()) && !poll.finished(Date.now()))
          s"Poll $id should be started at first\n"
        else {
          poll.stopDate.map { _ =>
            s"Poll $id would stop by itself\n"
          }.getOrElse({
            PollRepo.setPoll(id, poll.copy(stopDate = Some(Date.now())))
            s"Poll $id has been stopped\n"
          })
        }
      }
      else "You can't stop not yours poll\n"
    }.getOrElse(s"Sorry, the poll with id: $id has not been found\n")
  }

  def showResult(id: Int): String = {
    PollRepo.getPoll(id).map { poll =>
      if (poll.finished(Date.now()))
        s"The poll result is ${poll.getResult}\n"
      else {
        if (poll.active(Date.now())) {
          if (poll.isAfterStop) s"You should wait till poll is stopped.\n"
          else s"The poll result is ${poll.getResult}\n"
        }
        else s"The poll is not started yet.\n"
      }
    }.getOrElse(s"Sorry, the poll with id: $id has not been found\n")
  }

  def beginContext(id: Int, user: User): String =
    PollRepo.getPoll(id).map { _ =>
      Context.setUserContext(user.id, id)
      s"You started working with poll $id\n"
    }.getOrElse(s"Sorry, the poll with id: $id has not been found\n")

  def endContext(user: User): String = {
    Context.getPollId(user.id).map { _ =>
      Context.deleteUserContext(user.id)
      "You stopped working with poll"
    }.getOrElse(s"You was not working with any polls\n")
  }

  def viewPoll(user: User): String =
    Context.getPollId(user.id).map { pollId =>
      val poll = PollRepo.getPoll(pollId).get
      poll.toString
    }.getOrElse(s"You was not working with any polls\n")

  def addQuestion(question: String, questionType: QuestionType, variants: Seq[String], user: User): String = {
    Context.getPollId(user.id).map { pollId =>
      val poll = PollRepo.getPoll(pollId).get
      if (poll.creator.id == user.id) {
        PollRepo.setPoll(pollId, poll.copy(questions =
          poll.addQuestion(Question(question, questionType, poll.isAnonymous, variants))))
        "Question has been added\n"
      }
      else "You can't operate with not yours poll\n"
    }.getOrElse(s"You was not working with any polls\n")
  }

  def deleteQuestion(id: Int, user: User): String = {
    Context.getPollId(user.id).map { pollId =>
      val poll = PollRepo.getPoll(pollId).get
      if (poll.creator.id == user.id) {
        poll.getQuestion(id).map { _ =>
          PollRepo.setPoll(pollId, poll.copy(questions = poll.deleteQuestion(id)))
          "Question has been deleted\n"
        }.getOrElse("This question doesn't exist\n")
      }
      else "You can't operate with not yours poll\n"
    }.getOrElse(s"You was not working with any polls\n")
  }

  def answerOpen(id: Int, answer: String, user: User): String = {
    Context.getPollId(user.id).map { pollId =>
      PollRepo.getPoll(pollId).filter(poll => poll.active(Date.now())).map { poll =>
        poll.getQuestion(id).map { question =>
          if (question.questionType != QuestionType.Open) s"Wrong answer type: type is ${question.questionType}\n"
          else
            question.votedUsers.find(votedUser => votedUser.id == user.id).map { _ =>
              "You can't vote twice\n"
            }.getOrElse({
              PollRepo.setPoll(pollId, poll.copy(questions =
                poll.addVoteQuestion(id, question.voteOpen(user, answer))))
              "Your answer has been added\n"
            }
            )
        }.getOrElse("There are no question with this id\n")
      }.getOrElse("The poll is not active\n")
    }.getOrElse(s"You was not working with any polls\n")
  }

  def answerInt(id: Int, indexes: Seq[Int], user: User): String = {
    Context.getPollId(user.id).map { pollId =>
      PollRepo.getPoll(pollId).filter(poll => poll.active(Date.now())).map { poll =>
        poll.getQuestion(id).map { question =>
          if (indexes.forall(index => index > question.variants.length || index < 0) ||
            indexes.distinct.length != indexes.length) "Wrong answer id\n"
          else
            question.questionType match {
              case QuestionType.Choice =>
                if (indexes.size != 1) s"Wrong answer type: type is ${question.questionType}\n"
                else question.votedUsers.find(votedUser => votedUser.id == user.id).map { _ =>
                  "You can't vote twice\n"
                }.getOrElse({
                  PollRepo.setPoll(pollId, poll.copy(questions =
                    poll.addVoteQuestion(id, question.voteChoice(user, question.variants(indexes.head)))))
                  "Your answer has been added\n"
                })
              case QuestionType.Multi =>
                question.votedUsers.find(votedUser => votedUser.id == user.id).map { _ =>
                "You can't vote twice\n"
              }.getOrElse({
                PollRepo.setPoll(pollId, poll.copy(questions =
                  poll.addVoteQuestion(id, question.voteMulti(user, indexes))))
                "Your answer has been added\n"
              })
              case QuestionType.Open =>
                if (indexes.size != 1) s"Wrong answer type: type is ${question.questionType}\n"
                else question.votedUsers.find(votedUser => votedUser.id == user.id).map { _ =>
                  "You can't vote twice\n"
                }.getOrElse({
                  PollRepo.setPoll(pollId, poll.copy(questions =
                    poll.addVoteQuestion(id, question.voteOpen(user, indexes.head.toString))))
                  "Your answer has been added\n"
                })
              case _ => s"Wrong answer type: type is ${question.questionType}\n"
            }
        }.getOrElse("There are no question with this id\n")
      }.getOrElse("The poll is not active\n")
    }.getOrElse(s"You was not working with any polls\n")
  }
}
