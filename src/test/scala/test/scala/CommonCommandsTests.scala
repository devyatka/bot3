package test.scala


import info.mukel.telegrambot4s.models.User
import org.scalatest._
import com.telegrambot._
import java.time.{LocalDateTime => Date}

class CommonCommandsTests extends FunSuite with Matchers with BeforeAndAfter {

  val user: User = User(id = 0, isBot =false, firstName ="Arina")

  before(PollRepo.clear())

  test("Parser should return right command") {
    CommandParser("/create_poll (name)") shouldBe a [Matcher.CreatePoll]
    CommandParser("/list") shouldBe a [Matcher.ListPolls]
    CommandParser("/delete_poll (0)") shouldBe a [Matcher.DeletePoll]
    CommandParser("/start_poll (0)") shouldBe a [Matcher.StartPoll]
    CommandParser("/stop_poll (0)") shouldBe a [Matcher.StopPoll]
    CommandParser("/result (0)") shouldBe a [Matcher.ShowResult]
    CommandParser("/blablabla (0)") shouldBe a [Matcher.Failure]
  }

  test("Polls should be deleted correctly") {
    Bot.response("/create_poll (first) (no) (afterstop) (14:04:18 18.04.18) (14:04:18 18.05.18)", user)
    PollRepo.isEmpty shouldBe false
    Bot.response("/delete_poll (0)", user)
    PollRepo.isEmpty shouldBe true
  }

  test("Command execution should return correct string") {
    Bot.response("/create_poll (name2)",user) shouldBe "Id of name2 poll is: 0"
    Bot.response("/list",user) shouldBe "There are following polls:\nId: 0 Name: name2\n"
    Bot.response("/start_poll (0)",user) shouldBe "Poll 0 has been started\n"
    Bot.response("/stop_poll (0)",user) shouldBe "Poll 0 has been stopped\n"
    Bot.response("/result (0)",user) shouldBe "The poll result is \n"
    Bot.response("/delete_poll (0)",user) shouldBe "Poll 'name2' with id 0 has been deleted\n"
  }

  test("Tests with equal names should have different ids") {
    Bot.response("/create_poll (new poll)",user) shouldBe "Id of new poll poll is: 0"
    Bot.response("/create_poll (new poll)",user) shouldBe "Id of new poll poll is: 1"
    Bot.response("/create_poll (new poll)",user) shouldBe "Id of new poll poll is: 2"
  }

  test("Id of poll should be correct") {
    Bot.response("/start_poll (121231)",user) shouldBe "Sorry, the poll with id: 121231 has not been found\n"
    Bot.response("/start_poll (273478263782648)",user) shouldBe "Invalid command"
    Bot.response("/start_poll (-12)",user) shouldBe "Invalid command"
  }

  test("Parser should not take bad arguments") {
    Bot.response("/delete_poll (qweqweqwe)",user) shouldBe "Invalid command"
    Bot.response("/start_poll (smth)",user) shouldBe "Invalid command"
    Bot.response("/stop_poll (string thing)",user) shouldBe "Invalid command"
    Bot.response("/result (asdajdsbks)",user) shouldBe "Invalid command"
    Bot.response("/result (23672362745)",user) shouldBe "Invalid command"
    Bot.response("/result ()",user) shouldBe "Invalid command"
    Bot.response("/result",user) shouldBe "Invalid command"
    Bot.response("/delete_poll",user) shouldBe "Invalid command"
    Bot.response("/blablabla",user) shouldBe "Invalid command"
  }

  test("State of poll should be correct in time") {
    Bot.response("/create_poll (test_state) (yes) (continuous) (14:04:18 18.11.18) (14:04:18 18.12.18)",user)
    PollRepo.getPoll(0).get.active(Date.parse("2015-08-04T10:11:30")) shouldBe false
    PollRepo.getPoll(0).get.finished(Date.parse("2015-08-04T10:11:30")) shouldBe false
    PollRepo.getPoll(0).get.active(Date.parse("2018-12-06T10:11:30")) shouldBe true
    PollRepo.getPoll(0).get.finished(Date.parse("2018-12-06T10:11:30")) shouldBe false
    PollRepo.getPoll(0).get.active(Date.parse("2019-12-06T10:11:30")) shouldBe false
    PollRepo.getPoll(0).get.finished(Date.parse("2019-12-06T10:11:30")) shouldBe true
  }

  test("State of poll without initial time should be correct"){
    Bot.response("/create_poll (test_state)",user)
    PollRepo.getPoll(0).get.active(Date.now()) shouldBe false
    PollRepo.getPoll(0).get.finished(Date.now()) shouldBe false

    Bot.response("/start_poll (0)",user)
    PollRepo.getPoll(0).get.active(Date.now()) shouldBe true
    PollRepo.getPoll(0).get.finished(Date.now()) shouldBe false

    Bot.response("/stop_poll (0)",user)
    PollRepo.getPoll(0).get.active(Date.now().plusSeconds(1)) shouldBe false
    PollRepo.getPoll(0).get.finished(Date.now()) shouldBe true
  }

  test("Starting poll that is already active should be incorrect"){
    Bot.response("/create_poll (test_state)",user)
    Bot.response("/start_poll (0)",user) shouldBe "Poll 0 has been started\n"
    Bot.response("/start_poll (0)",user) shouldBe "The poll with id 0 is already started\n"
  }

  test("Starting and stopping poll with initial time should be incorrect"){
    Bot.response("/create_poll (test_state2) (yes) (continuous) (14:04:18 18.11.18) (14:04:18 18.12.18)",user)
    Bot.response("/start_poll (0)",user) shouldBe "Poll 0 would start by itself\n"
    Bot.response("/stop_poll (0)",user) shouldBe "Poll 0 should be started at first\n"
  }

  test("Stopping poll that is inactive should be incorrect"){
    Bot.response("/create_poll (test_state)",user)
    Bot.response("/stop_poll (0)",user) shouldBe "Poll 0 should be started at first\n"
    Bot.response("/start_poll (0)",user)
    Bot.response("/stop_poll (0)",user)
    Bot.response("/stop_poll (0)",user) shouldBe "The Poll 0 is already finished\n"
  }

  test("Result should be shown at the right time"){
    Bot.response("/create_poll (name)",user)
    Bot.response("/result (0)",user) shouldBe "The poll is not started yet.\n"
    Bot.response("/start_poll (0)",user)
    Bot.response("/result (0)",user) shouldBe "The poll result is \n"
    Bot.response("/create_poll (name1) (no) (afterstop)",user)
    Bot.response("/result (1)",user) shouldBe "The poll is not started yet.\n"
    Bot.response("/start_poll (1)",user)
    Bot.response("/result (1)",user) shouldBe "You should wait till poll is stopped.\n"
    Bot.response("/stop_poll (1)",user)
    Bot.response("/result (1)",user) shouldBe "The poll result is \n"
  }

  test("Poll id should be unique"){
    Bot.response("/create_poll (name)",user)
    Bot.response("/delete_poll (0)",user)
    Bot.response("/create_poll (name1)",user) shouldBe "Id of name1 poll is: 1"
  }
}
