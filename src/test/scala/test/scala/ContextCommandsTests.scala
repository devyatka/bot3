package test.scala

import info.mukel.telegrambot4s.models.User
import com.telegrambot._
import org.scalatest._
import org.scalatest.FunSuite

class ContextCommandsTests extends FunSuite with Matchers with BeforeAndAfter {

  val users: Seq[User] =
    Seq(User(id =0, isBot = false, firstName = "Arina"),
    User(id = 1, isBot = false, firstName = "Masha"))

  before {
    PollRepo.clear()
    Context.clear()
    Bot.response("/create_poll (Arina) (yes) (afterstop)",users.head)
    Bot.response("/create_poll (Masha) (no) (continuous)",users(1))
  }

  test("Commands should be parsed correctly") {
    val begin = CommandParser("/begin (0)")
    begin shouldBe a[Matcher.BeginContext]

    val end = CommandParser("/end")
    end shouldBe a[Matcher.EndContext]

    val view = CommandParser("/view")
    view shouldBe a[Matcher.ViewPoll]

    val add_question = CommandParser("/add_question (question1) (open)")
    add_question shouldBe a[Matcher.AddQuestion]

    val delete_question = CommandParser("/delete_question (0)")
    delete_question shouldBe a[Matcher.DeleteQuestion]

    val answer = CommandParser("/answer (0) (hello)")
    answer shouldBe a[Matcher.AnswerOpen]
  }

  test("Command arguments should be correct") {
    Bot.response("/add_question",users.head) shouldBe "Invalid command"
    Bot.response("/begin",users.head) shouldBe "Invalid command"
    Bot.response("/begin (23672362745)",users.head) shouldBe "Invalid command"
    Bot.response("/end (1)",users.head) shouldBe "Invalid command"
    Bot.response("/view (1)",users.head) shouldBe "Invalid command"
  }

  test("Begin should work right") {
    Bot.response("/begin (0)",users.head) shouldBe s"You started working with poll 0\n"
    Bot.response("/begin (0)",users(1)) shouldBe s"You started working with poll 0\n"
    Bot.response("/view",users.head) should not be "You was not working with any polls\n"
    Bot.response("/view",users(1)) should not be "You was not working with any polls\n"
    Bot.response("/begin (2)",users.head) shouldBe "Sorry, the poll with id: 2 has not been found\n"
    Bot.response("/begin (2)",users(1)) shouldBe "Sorry, the poll with id: 2 has not been found\n"
    Bot.response("/end",users.head)
    Bot.response("/begin",users(1))
  }

  test("End should work right") {
    Bot.response("/end",users.head) shouldBe "You was not working with any polls\n"
    Bot.response("/begin (0)",users.head)
    Bot.response("/begin (0)",users(1))
    Bot.response("/end",users.head) shouldBe "You stopped working with poll"
    Bot.response("/end",users(1)) shouldBe "You stopped working with poll"
    Bot.response("/view",users.head) shouldBe "You was not working with any polls\n"
    Bot.response("/view",users(1)) shouldBe "You was not working with any polls\n"
  }

  test("View should work right") {
    Bot.response("/begin (0)",users.head)
    Bot.response("/view",users.head) shouldBe
      "\nName: Arina" +
        "\nAnonymity: true" +
        "\nIs after stop: true" +
        "\nStart date:\n\tdate: not set\n\ttime: not set" +
        "\nStop date:\n\tdate: not set\n\ttime: not set\n"
    Bot.response("/view",users(1)) shouldBe "You was not working with any polls\n"
    Bot.response("/end",users.head)
  }

  test("Beginning another poll should end the previous") {
    Bot.response("/begin (0)",users.head) shouldBe "You started working with poll 0\n"
    Bot.response("/add_question (q1) (open)",users.head) shouldBe "Question has been added\n"
    Bot.response("/begin (1)",users.head) shouldBe "You started working with poll 1\n"
    Bot.response("/add_question (q1) (open)",users.head) shouldBe "You can't operate with not yours poll\n"
  }

  test("User should operate only with his polls") {
    Bot.response("/begin (0)",users(1))
    Bot.response("/add_question (q1) (open)",users(1)) shouldBe "You can't operate with not yours poll\n"
    Bot.response("/delete_question (0)",users(1)) shouldBe "You can't operate with not yours poll\n"
  }

  test("Question with type Open should not have variants") {
    Bot.response("/begin (0)",users.head)
    Bot.response("/add_question (q1) (open) (variant)",users.head) shouldBe "Invalid command"
    Bot.response("/add_question (q1) (open)",users.head) shouldBe "Question has been added\n"
  }

  test("Question with type Choice and Multi should have variants") {
    Bot.response("/begin (0)",users.head)
    Bot.response("/add_question (qwe1) (choice) (a) (b) (c)",users.head) shouldBe "Question has been added\n"
    Bot.response("/add_question (qwe2) (choice)",users.head) shouldBe "Invalid command"
    Bot.response("/add_question (qwe1) (multi) (a) (b) (c) (d) (e)",users.head) shouldBe "Question has been added\n"
    Bot.response("/add_question (qwe2) (multi)",users.head) shouldBe "Invalid command"
  }

  test("Deleting questions should work correct") {
    Bot.response("/begin (0)",users.head)
    Bot.response("/delete_question (123)",users.head) shouldBe "This question doesn't exist\n"
    Bot.response("/delete_question (0)",users.head) shouldBe "This question doesn't exist\n"
    Bot.response("/create_poll (name)",users.head)
    Bot.response("/add_question (qw1) (open)",users.head)
    Bot.response("/delete_question (0)",users.head) shouldBe "Question has been deleted\n"
  }

  test("Answering on questions with choice should be correct") {
    Bot.response("/begin (0)",users.head)
    Bot.response("/add_question (a1) (choice) (aa) (bb) (cc)",users.head)
    Bot.response("/start_poll (0)",users.head)
    Bot.response("/begin (0)",users(1))
    Bot.response("/answer (0) (aaaa)",users(1)) shouldBe "Wrong answer type: type is Choice\n"
    Bot.response("/answer (0) (0)",users(1)) shouldBe "Your answer has been added\n"
  }

  test("Answering on questions with multi choice should be correct") {
    Bot.response("/begin (0)",users.head)
    Bot.response("/add_question (a1) (multi) (aa) (bb) (cc)",users.head)
    Bot.response("/start_poll (0)",users.head)
    Bot.response("/begin (0)",users(1))
    Bot.response("/answer (0) (aaaa)",users(1)) shouldBe "Wrong answer type: type is Multi\n"
    Bot.response("/answer (0) (0) (1)",users(1)) shouldBe "Your answer has been added\n"
  }

  test("Question type should parse correct") {
    CommandsImplementation.beginContext(0,users.head)
    Bot.response("/add_question (question1) (open)", users.head)
    Bot.response("/add_question (question1) (choice) (otstoy) (mda)", users.head)
    Bot.response("/add_question (question1) (multi) (heh) (huh) (hah)", users.head)
    PollRepo.getPoll(0).get.getQuestion(0).get.questionType shouldBe QuestionType.Open
    PollRepo.getPoll(0).get.getQuestion(1).get.questionType shouldBe QuestionType.Choice
    PollRepo.getPoll(0).get.getQuestion(2).get.questionType shouldBe QuestionType.Multi
  }

  test("Answering on non-active question should not be allowed") {
    Bot.response("/begin (0)",users(1))
    Bot.response("/begin (0)",users.head)
    Bot.response("/add_question (a1) (open)",users.head)
    Bot.response("/answer (0) (hello)",users(1)) shouldBe "The poll is not active\n"
    Bot.response("/start_poll (0)",users.head) shouldBe "Poll 0 has been started\n"
    Bot.response("/answer (0) (hello)",users(1)) shouldBe "Your answer has been added\n"
  }

  test("Answering twice should be not allowed") {
    Bot.response("/begin (0)",users.head)
    Bot.response("/add_question (a1) (open)",users.head)
    Bot.response("/start_poll (0)",users.head) shouldBe "Poll 0 has been started\n"
    Bot.response("/answer (0) (hello)",users.head) shouldBe "Your answer has been added\n"
    Bot.response("/answer (0) (hello)",users.head) shouldBe "You can't vote twice\n"
  }
  test("Anonymity should work") {
    Bot.response("/begin (0)",users.head)
    Bot.response("/add_question (a1) (open)",users.head)
    Bot.response("/start_poll (0)",users.head)
    Bot.response("/answer (0) (hello)",users.head)
    PollRepo.getPoll(0).get.getQuestion(0).get.userVotes contains users.head shouldBe false
  }

  test("Non Anonymity should work") {
    Bot.response("/begin (1)",users(1))
    Bot.response("/add_question (a1) (open)",users(1))
    Bot.response("/start_poll (1)",users(1))
    Bot.response("/answer (0) (hello)",users(1))
    PollRepo.getPoll(1).get.getQuestion(0).get.userVotes contains users(1) shouldBe true
  }
}
